# About the project

**AnimeBrowserPaging** is a revamped version of [AnimeBrowser](https://bitbucket.org/catness/animebrowser) with paging, following the [Android Paging codelab](https://developer.android.com/codelabs/android-paging). (Actually, only the 1st half, because the nature of this app doesn't call for using separators or local caching.)

For learning/debugging purposes, I didn't remove the old non-paging code but added a setting (in the preferences) to switch between the paging and non-paging version.

The used API is [Jikan - an Unofficial MyAnimeList API](https://jikan.docs.apiary.io/), specifically, searching for anime and manga by keywords and/or genres, with several sorting options. More about it on the original [AnimeBrowser](https://bitbucket.org/catness/animebrowser) page.

The apk is here: [animebrowser_paging.apk](apk/animebrowser_paging.apk).
___

My blog: [Cat's Mysterious Box](http://catness.org/logbook/)



