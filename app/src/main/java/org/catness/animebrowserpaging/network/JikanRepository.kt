package org.catness.animebrowserpaging.network

import android.util.Log
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import kotlinx.coroutines.flow.Flow

class JikanRepository(private val service: JikanApiService) {
    private val LOG_TAG: String = this.javaClass.simpleName

    /**
     * Search results, exposed as a stream of data that will emit
     * every time we get more data from the network.
     */
    fun getSearchResultStream(
        type: String,
        searchString: String,
        sortBy: String,
        sort: String,
        genre: String
    )
            : Flow<PagingData<AnimeNet>> {
        return Pager(
            config = PagingConfig(pageSize = PAGESIZE, enablePlaceholders = false),
            pagingSourceFactory = {
                JikanPagingSource(
                    service,
                    type,
                    searchString,
                    sortBy,
                    sort,
                    genre
                )
            }
        ).flow
    }
}
