package org.catness.animebrowserpaging.network

import android.util.Log
import androidx.paging.PagingSource
import retrofit2.HttpException
import java.io.IOException

// page API is 1 based
private const val STARTING_PAGE_INDEX = 1

class JikanPagingSource(
    private val service: JikanApiService,
    private val type: String,
    private val searchString: String,
    private val sortBy: String,
    private val sort: String,
    private val genre: String
) : PagingSource<Int, AnimeNet>() {
    private val LOG_TAG: String = this.javaClass.simpleName

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, AnimeNet> {
        val position = params.key ?: STARTING_PAGE_INDEX
        return try {
            Log.i(
                LOG_TAG,
                "getResults searchString=$searchString type=$type sortBy=$sortBy sort=$sort genre=$genre position=$position"
            )
            val response = service.getAnimePaging(
                type,
                searchString,
                sortBy,
                sort,
                genre,
                position,
                PAGESIZE
            )

            val items = response.results
            items.forEach { Log.i(LOG_TAG, "result=${it}") }
            LoadResult.Page(
                data = items,
                prevKey = if (position == STARTING_PAGE_INDEX) null else position - 1,
                nextKey = if (items.isEmpty()) null else position + 1
            )
        } catch (exception: IOException) {
            LoadResult.Error(exception)
        } catch (exception: HttpException) {
            LoadResult.Error(exception)
        }
    }
}
