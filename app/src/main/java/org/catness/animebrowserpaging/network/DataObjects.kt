package org.catness.animebrowserpaging.network

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

data class AnimeNet(
    val title: String,
    val type: String,
    val url: String,
    @Json(name = "image_url") val imageUrl: String,
    val synopsis: String,
    @Json(name = "start_date") val startDateFull: String? = "",
    @Json(name = "end_date") val endDateFull: String? = "",
    val episodes: Int? = 0,
    val chapters: Int? = 0,
    val volumes: Int? = 0,
    val score: Float? = 0f,
    val mal_id: Int
) {
    val malId
        get() = mal_id.toString()  // for debugging
    val startDate
        get() = fixDate(startDateFull?.substring(0, 10) ?: "")
    val endDate
        get() = fixDate(endDateFull?.substring(0, 10) ?: "")

    fun fixDate(date: String): String {
        // dates older than 1970(?) are returned like they're in the next century
        // so if the date is far in the future, we replace the starting "20" with "19"
        return when (date) {
            "" -> ""
            else -> if (date.substring(0, 4).toInt() > 2030) "19" + date.substring(2) else date
        }
    }
}

data class AnimeFeed(
    val results: List<AnimeNet>,
    val nextPage: Int? = null
)

data class AnimeFeedPaging(
    @Json(name = "results") val results: List<AnimeNet> = emptyList(),
    val nextPage: Int? = null
)