package org.catness.animebrowserpaging.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

enum class ApiStatus { LOADING, ERROR, DONE }

private const val BASE_URL = "https://api.jikan.moe/v3/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .baseUrl(BASE_URL)
    .build()

// the max page size for the API is 50
val PAGESIZE: Int = 20

interface JikanApiService {
    @GET("search/{type}/")
    fun getAnime(
        @Path("type") type: String,
        @Query("q") search: String?,
        @Query("order_by") sortBy: String,
        @Query("sort") sort: String,
        @Query("genre") genre: String?,
        @Query("page") page: Int,
        @Query("limit") limit: Int
    ):
            Deferred<AnimeFeed>

    @GET("search/{type}/")
    suspend fun getAnimePaging(
        @Path("type") type: String,
        @Query("q") search: String?,
        @Query("order_by") sortBy: String,
        @Query("sort") sort: String,
        @Query("genre") genre: String?,
        @Query("page") page: Int,
        @Query("limit") limit: Int
    ):
            AnimeFeedPaging

    companion object {
        // the way it's initialized in the paging example
        fun create(): JikanApiService {
            // logger is optional but useful for debugging!
            val logger = HttpLoggingInterceptor()
            logger.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient.Builder()
                .addInterceptor(logger)
                .build()

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                //.addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()
                .create(JikanApiService::class.java)
        }
    }
}

object JikanApi {
    // the way it's initialized in the old retrofit example
    val retrofitService: JikanApiService by lazy {
        retrofit.create(JikanApiService::class.java)
    }

}
