package org.catness.animebrowserpaging.results

import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter

class AnimeLoadStateAdapter(
        private val retry: () -> Unit
) : LoadStateAdapter<AnimeLoadStateViewHolder>() {
    override fun onBindViewHolder(holder: AnimeLoadStateViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }
    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): AnimeLoadStateViewHolder {
        return AnimeLoadStateViewHolder.create(parent, retry)
    }
}
