package org.catness.animebrowserpaging.results

import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.catness.animebrowserpaging.R
import org.catness.animebrowserpaging.databinding.ListItemAnimePagingBinding
import org.catness.animebrowserpaging.network.AnimeNet

class AnimeViewHolder private constructor(val binding: ListItemAnimePagingBinding) :
    RecyclerView.ViewHolder(binding.root) {
    private val LOG_TAG: String = this.javaClass.simpleName
    private var anime: AnimeNet? = null

    init {
        binding.link.setOnClickListener {
            anime?.url?.let { url ->
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                binding.link.context.startActivity(intent)
            }
        }
    }

    fun bind(anime: AnimeNet?) {
        Log.i(LOG_TAG, "bind: ${anime}")
        if (anime == null) {
            val resources = itemView.resources
            binding.animeTitle.text = resources.getString(R.string.loading)
        } else {
            showAnimeData(anime)
        }
    }

    private fun showAnimeData(anime: AnimeNet) {
        this.anime = anime

        Log.i(LOG_TAG, "showAnimeData: ${anime} binding=${binding}")
        binding.anime = anime
        binding.executePendingBindings()
    }

    companion object {
        fun from(parent: ViewGroup): AnimeViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ListItemAnimePagingBinding.inflate(layoutInflater, parent, false)
            return AnimeViewHolder(binding)
        }
    }

}
