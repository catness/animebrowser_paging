package org.catness.animebrowserpaging.results

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import org.catness.animebrowserpaging.databinding.ListItemAnimeBinding
import org.catness.animebrowserpaging.databinding.ListItemAnimePagingBinding
import org.catness.animebrowserpaging.network.AnimeNet

class ResultsAdapterPaging : PagingDataAdapter<AnimeNet, ViewHolder>(COMPARATOR) {
    private val LOG_TAG: String = this.javaClass.simpleName

    init {
        Log.i(LOG_TAG, "init")
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        Log.i(LOG_TAG, "onAttached")
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        Log.i(LOG_TAG, "onCreateViewHolder $viewType")
        return AnimeViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Log.i(LOG_TAG, "onBindNewHolder: position=$position")
        val animeItem = getItem(position)
        if (animeItem != null) {
            (holder as AnimeViewHolder).bind(animeItem)
        }
    }

    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<AnimeNet>() {
            override fun areItemsTheSame(oldItem: AnimeNet, newItem: AnimeNet): Boolean =
                oldItem.mal_id == newItem.mal_id

            override fun areContentsTheSame(oldItem: AnimeNet, newItem: AnimeNet): Boolean =
                oldItem == newItem
        }
    }
}
