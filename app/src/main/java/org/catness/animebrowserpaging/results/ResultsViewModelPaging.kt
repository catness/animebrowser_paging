package org.catness.animebrowserpaging.results

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.forEach
import org.catness.animebrowserpaging.network.AnimeNet
import org.catness.animebrowserpaging.network.JikanRepository

class ResultsViewModelPaging(private val repository: JikanRepository) : ViewModel() {
    private val LOG_TAG: String = this.javaClass.simpleName
    private var currentQueryValue: String? = null

    private var currentSearchResult: Flow<PagingData<AnimeNet>>? = null

    fun searchAnime(
        type: String,
        searchString: String,
        sortBy: String,
        sort: String,
        genre: String
    ): Flow<PagingData<AnimeNet>> {
        val lastResult = currentSearchResult
        var queryString = type + ":" + searchString + ":" + sortBy + ":" + sort + ":" + genre
        if (queryString == currentQueryValue && lastResult != null) {
            Log.i(LOG_TAG, "searchAnime: return last result")
            return lastResult
        }
        currentQueryValue = queryString
        val newResult: Flow<PagingData<AnimeNet>> =
            repository.getSearchResultStream(type, searchString, sortBy, sort, genre)
                .cachedIn(viewModelScope)
        currentSearchResult = newResult
        Log.i(LOG_TAG, "searchAnime: return new result ${newResult}")
        return newResult
    }
}
