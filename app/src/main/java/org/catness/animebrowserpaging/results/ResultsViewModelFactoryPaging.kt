package org.catness.animebrowserpaging.results

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import org.catness.animebrowserpaging.network.JikanRepository

/**
 * Factory for ViewModels
 */
class ResultsViewModelFactoryPaging(private val repository: JikanRepository) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ResultsViewModelPaging::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return ResultsViewModelPaging(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
