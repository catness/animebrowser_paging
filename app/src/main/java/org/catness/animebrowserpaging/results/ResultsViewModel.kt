package org.catness.animebrowserpaging.results

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.catness.animebrowserpaging.animeDebugString
import org.catness.animebrowserpaging.network.AnimeNet
import org.catness.animebrowserpaging.network.ApiStatus
import org.catness.animebrowserpaging.network.JikanApi
import org.catness.animebrowserpaging.network.PAGESIZE
import java.util.*

class ResultsViewModel(
    private val searchString: String?,
    private val type: String,
    private val sortBy: String,
    private val sort: String,
    private val genre: String?
) : ViewModel() {
    private val LOG_TAG: String = this.javaClass.simpleName

    private val _status = MutableLiveData<ApiStatus>()

    // The external immutable LiveData for the request status
    val status: LiveData<ApiStatus>
        get() = _status

    // The internal MutableLiveData String that stores the most recent response
    private val _response = MutableLiveData<String>()

    // The external immutable LiveData for the response String
    val response: LiveData<String>
        get() = _response

    private val _anime = MutableLiveData<MutableList<AnimeNet>>()
    val anime: LiveData<MutableList<AnimeNet>>
        get() = _anime

    private var page: Int = 1

    private val _moreResults = MutableLiveData<Boolean>()
    val moreResults: LiveData<Boolean>
        get() = _moreResults

    private val _noResults = MutableLiveData<Boolean>()
    val noResults: LiveData<Boolean>
        get() = _noResults

    private val _updated = MutableLiveData<Boolean>()
    val updated: LiveData<Boolean>
        get() = _updated


    // Create a Coroutine scope using a job to be able to cancel when needed
    private var viewModelJob = Job()

    // the Coroutine runs using the Main (UI) dispatcher
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    init {
        _updated.value = false
        _moreResults.value = false
        _noResults.value = false
        getResults(searchString, type, sortBy, sort, genre, page)
    }

    private fun getResults(
        searchString: String?,
        type: String,
        sortBy: String,
        sort: String,
        genre: String?,
        page: Int
    ) {
        //_response.value = "List of results for $searchString $type"

        coroutineScope.launch {
            // Get the Deferred object for our Retrofit request
            Log.i(
                LOG_TAG,
                "getResults: $searchString type=$type sortBy=$sortBy sort=$sort genre=$genre page=$page"
            )
            var getResultDeferred = JikanApi.retrofitService.getAnime(
                type,
                searchString,
                sortBy,
                sort,
                genre,
                page,
                PAGESIZE
            )
            try {
                _moreResults.value = false
                _status.value = ApiStatus.LOADING
                var listResult = getResultDeferred.await()
                _status.value = ApiStatus.DONE
                var str = animeDebugString(listResult.results)
                Log.i(LOG_TAG, str)
                _response.value = str
                _moreResults.value = listResult.results.size >= PAGESIZE
                if (_anime.value == null) {
                    _anime.value = listResult.results.toMutableList()
                    //Log.i(LOG_TAG, "first result")
                } else {
                    _anime.value!!.addAll(listResult.results)
                    //Log.i(LOG_TAG, "added result ; now list is: ")
                    //Log.i(LOG_TAG, animeDebugString(Collections.unmodifiableList(_anime.value)))
                }
                _noResults.value = _anime.value!!.size == 0
                _updated.value = true
            } catch (e: Exception) {
                _status.value = ApiStatus.ERROR
                _moreResults.value = false
                _response.value = "Failure: ${e.message}"
                Log.i(LOG_TAG, "Failure: ${e.message}")
            }
        }
    }

    fun moreClicked() {
        getResults(searchString, type, sortBy, sort, genre, ++page)
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

}