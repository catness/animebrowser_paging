package org.catness.animebrowserpaging.results

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.launch
import org.catness.animebrowserpaging.R.layout.fragment_results_paging
import org.catness.animebrowserpaging.databinding.FragmentResultsPagingBinding
import org.catness.animebrowserpaging.network.JikanApi
import org.catness.animebrowserpaging.network.JikanApiService
import org.catness.animebrowserpaging.network.JikanRepository
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter

class ResultsFragmentPaging : Fragment() {
    private val LOG_TAG: String = this.javaClass.simpleName

    private lateinit var binding: FragmentResultsPagingBinding
    private lateinit var viewModel: ResultsViewModelPaging
    private val adapter = ResultsAdapterPaging()
    private var searchJob: Job? = null

    private fun search(
        type: String,
        searchString: String,
        sortBy: String,
        sort: String,
        genre: String
    ) {
        searchJob?.cancel()
        searchJob = lifecycleScope.launch {
            viewModel.searchAnime(type, searchString, sortBy, sort, genre).collectLatest {
                adapter.submitData(it)
                adapter.notifyDataSetChanged()
            }
        }
    }

    //@InternalCoroutinesApi
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater, fragment_results_paging, container, false
        )
        binding.lifecycleOwner = this

        val arguments = arguments?.let { ResultsFragmentPagingArgs.fromBundle(it) }
        var searchString = arguments?.searchString ?: ""
        var type = requireArguments().getString("type") ?: "anime"
        var sortBy = requireArguments().getString("sortBy") ?: "score"
        var sort = requireArguments().getString("sort") ?: "descending"
        var genre = requireArguments().getString("genre") ?: ""
        Log.i(
            LOG_TAG,
            "search ${searchString} type ${type} sortBy ${sortBy} sort ${sort} genre=${genre}"
        )

        val viewModelFactory =
            ResultsViewModelFactoryPaging(JikanRepository(JikanApiService.create()))
        viewModel =
            ViewModelProvider(
                this, viewModelFactory
            ).get(ResultsViewModelPaging::class.java)

        binding.retryButton.setOnClickListener { adapter.retry() }

        // add dividers between RecyclerView's row items
        val decoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        binding.listp.addItemDecoration(decoration)
        binding.listp.adapter = adapter.withLoadStateFooter(
            // header = AnimeLoadStateAdapter { adapter.retry() },
            footer = AnimeLoadStateAdapter { adapter.retry() }
        )
        adapter.addLoadStateListener { loadState ->
            // Only show the list if refresh succeeds.
            binding.listp.isVisible = loadState.source.refresh is LoadState.NotLoading
            // Show loading spinner during initial load or refresh.
            binding.progressBar.isVisible = loadState.source.refresh is LoadState.Loading
            // Show the retry state if initial load or refresh fails.
            binding.retryButton.isVisible = loadState.source.refresh is LoadState.Error

            // Toast on any error, regardless of whether it came from RemoteMediator or PagingSource
            val errorState = loadState.source.append as? LoadState.Error
                ?: loadState.source.prepend as? LoadState.Error
                ?: loadState.append as? LoadState.Error
                ?: loadState.prepend as? LoadState.Error
            errorState?.let {
                Toast.makeText(
                    context,
                    "\uD83D\uDE28 Wooops ${it.error}",
                    Toast.LENGTH_LONG
                ).show()
            }
        }

        // Scroll to top when the list is refreshed from network.
        search(type, searchString, sortBy, sort, genre)
        lifecycleScope.launch {
            adapter.loadStateFlow
                // Only emit when REFRESH LoadState for RemoteMediator changes.
                .distinctUntilChangedBy { it.refresh }
                // Only react to cases where Remote REFRESH completes i.e., NotLoading.
                .filter { it.refresh is LoadState.NotLoading }
                .collect { binding.listp.scrollToPosition(0) }
        }





        return binding.root
    }

}