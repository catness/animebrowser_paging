package org.catness.animebrowserpaging.results

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_item_anime.view.*
import org.catness.animebrowserpaging.databinding.ListItemAnimeBinding
import org.catness.animebrowserpaging.network.AnimeNet


class ResultsAdapter(private val onClickListener: OnClickListener) :
    ListAdapter<AnimeNet, ResultsAdapter.ViewHolder>(DiffCallback()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.itemView.link.setOnClickListener {
            onClickListener.onClick(item)
        }
        holder.bind(item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(val binding: ListItemAnimeBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: AnimeNet) {
            binding.anime = item
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemAnimeBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }

    /**
     * Custom listener that handles clicks on [RecyclerView] items.  Passes the anime
     * associated with the current item to the [onClick] function.
     * @param clickListener lambda that will be called with the current anime
     */
    class OnClickListener(val clickListener: (anime: AnimeNet) -> Unit) {
        fun onClick(anime: AnimeNet) = clickListener(anime)
    }
}

class DiffCallback : DiffUtil.ItemCallback<AnimeNet>() {

    override fun areItemsTheSame(oldItem: AnimeNet, newItem: AnimeNet): Boolean {
        return oldItem.mal_id == newItem.mal_id
    }

    override fun areContentsTheSame(oldItem: AnimeNet, newItem: AnimeNet): Boolean {
        return oldItem == newItem
    }

}
