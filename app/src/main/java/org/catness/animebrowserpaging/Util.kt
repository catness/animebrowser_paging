package org.catness.animebrowserpaging

import org.catness.animebrowserpaging.network.AnimeNet

fun animeDebugString(anime: List<AnimeNet>) : String {
    return anime.joinToString { it.malId + " " + it.title }
}