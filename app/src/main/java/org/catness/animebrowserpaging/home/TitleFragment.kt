package org.catness.animebrowserpaging.home

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.view.children
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.preference.PreferenceManager
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import org.catness.animebrowserpaging.R
import org.catness.animebrowserpaging.databinding.FragmentTitleBinding
import org.catness.animebrowserpaging.network.genreAnime
import org.catness.animebrowserpaging.network.genreManga

class TitleFragment : Fragment() {
    private val LOG_TAG: String = this.javaClass.simpleName
    private var paging: Boolean = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentTitleBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_title, container, false
        )

        val viewModel: TitleViewModel by lazy {
            ViewModelProvider(this).get(TitleViewModel::class.java)
        }

        binding.viewModel = viewModel
        var chips: ChipGroup = binding.searchType
        chips.check(binding.anime.id)

        binding.score.setChecked(true)

        binding.descending.setChecked(true)

        binding.ascending.setOnCheckedChangeListener { button, isChecked ->
            binding.descending.setChecked(!isChecked)
        }

        binding.descending.setOnCheckedChangeListener { button, isChecked ->
            binding.ascending.setChecked(!isChecked)
        }

        val chipGroupAnime = binding.genreAnime
        val inflator = LayoutInflater.from(chipGroupAnime.context)
        val children = genreAnime.mapIndexed { idx, name ->
            val chip = inflator.inflate(R.layout.genre, chipGroupAnime, false) as Chip
            chip.text = name
            chip.tag = (idx + 1).toString()
            chip
        }
        chipGroupAnime.removeAllViews()
        var cnt = 0
        for (chip in children) {
            chipGroupAnime.addView(chip)
            cnt++
        }
        chipGroupAnime.visibility = VISIBLE

        val chipGroupManga = binding.genreManga
        val inflator1 = LayoutInflater.from(chipGroupManga.context)
        val children1 = genreManga.mapIndexed { idx, name ->
            val chip = inflator1.inflate(R.layout.genre, chipGroupManga, false) as Chip
            chip.text = name
            chip.tag = (idx + 1).toString()
            chip
        }
        chipGroupManga.removeAllViews()
        for (chip in children1) {
            chipGroupManga.addView(chip)
        }
        chipGroupManga.requestLayout()
        chipGroupManga.visibility = GONE


        binding.anime.setOnCheckedChangeListener { button, isChecked ->
            chipGroupAnime.visibility = if (isChecked) VISIBLE else GONE
            chipGroupManga.visibility = if (isChecked) GONE else VISIBLE
        }

        binding.manga.setOnCheckedChangeListener { button, isChecked ->
            chipGroupManga.visibility = if (isChecked) VISIBLE else GONE
            chipGroupAnime.visibility = if (isChecked) GONE else VISIBLE
        }

        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        paging = prefs.getBoolean("Paging", true)
        Log.i(LOG_TAG,"paging=$paging (loaded)")

        PreferenceManager.getDefaultSharedPreferences(context)
            .registerOnSharedPreferenceChangeListener { sharedPreferences, key ->
                if (key == "Paging") {
                    paging = prefs.getBoolean("Paging", true)
                    Log.i(LOG_TAG,"paging=$paging (changed)")
                }
            }



        viewModel.navigateToSearch.observe(viewLifecycleOwner,
            Observer<Boolean> { navigate ->
                if (navigate) {
                    // Hide the keyboard.
                    val imm =
                        activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(requireView().getWindowToken(), 0)

                    var type: String = when (chips.checkedChipId) {
                        binding.manga.id -> "manga"
                        binding.anime.id -> "anime"
                        else -> "anime"
                    }

                    var sortBy: String = when (binding.sortBy.checkedChipId) {
                        binding.score.id -> "score"
                        binding.title.id -> "title"
                        binding.start.id -> "start_date"
                        binding.end.id -> "end_date"
                        else -> "title"
                    }

                    var sort: String =
                        if (binding.ascending.isChecked) "ascending" else "descending"

                    var genre =
                        if (type == "anime") getGenres(chipGroupAnime) else getGenres(chipGroupManga)

                    var query = viewModel.searchString.value

                    Log.i(LOG_TAG,"Before sending query: paging=$paging")
                    if (paging) {
                        this.findNavController().navigate(
                            TitleFragmentDirections.actionTitleFragmentToResultsFragmentPaging
                                (if (query == "") null else query, type, sortBy, sort, genre)
                        )
                    }
                    else {
                        this.findNavController().navigate(
                            TitleFragmentDirections.actionTitleFragmentToResultsFragment
                                (if (query == "") null else query, type, sortBy, sort, genre)
                        )
                    }
                    viewModel.onNavigatedToSearch()
                }
            })

        return binding.root
    }

    private fun getGenres(group: ChipGroup): String? {
        val selectedChips = group.children
            .filter { (it as Chip).isChecked }
            .map { (it as Chip).tag.toString() }
        val selected = selectedChips.joinToString(",")
        Log.i(LOG_TAG, "selected genres: $selected")
        return if (selected == "") null else selected
    }


}